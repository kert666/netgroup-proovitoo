﻿using Proovitöö_Netgroup.Entity.Portfolio;
using System.Collections.ObjectModel;
using System.Linq;

namespace Proovitöö_Netgroup
{
    public class Portfolio : PortfolioBase
    {
        public Portfolio()
        {
            FillPortfolioWithData();
        }

        public Stock StockPosition(string securityKey)
        {
            return Stocks.FirstOrDefault(x => x.SecurityKey == securityKey);
        }

        public Etf EtfPosition(string securityKey)
        {
            return Etfs.FirstOrDefault(x => x.SecurityKey == securityKey);
        }

        public void FillPortfolioWithData()
        {
            Stocks = new Collection<Stock>();
            Etfs = new Collection<Etf>();

            OtherAsset = new OtherAsset
            {
                MarketValue = 1000000.00m
            };

            Stocks.Add(new Stock
            {
                SecurityKey = "SBC",
                Quantity = 100,
                Price = 85.00m
            });

            Stocks.Add(new Stock
            {
                SecurityKey = "SBB",
                Quantity = 10,
                Price = 55.00m
            });

            Etfs.Add(new Etf
            {
                SecurityKey = "EBB",
                Quantity = 10,
                Price = 55.00m
            });

            Etfs.Add(new Etf
            {
                SecurityKey = "EBC",
                Quantity = 2000,
                Price = 25.00m
            });
        }
    }
}
