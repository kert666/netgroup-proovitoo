﻿using System.Collections.ObjectModel;

namespace Proovitöö_Netgroup.Entity.Portfolio
{
    public abstract class PortfolioBase
    {
        public Collection<Stock> Stocks { get; set; }

        public Collection<Etf> Etfs { get; set; }

        public OtherAsset OtherAsset { get; set; }
    }
}
