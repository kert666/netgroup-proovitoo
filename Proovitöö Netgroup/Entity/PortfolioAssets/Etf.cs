﻿namespace Proovitöö_Netgroup
{
    public class Etf
    {
        public string SecurityKey { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }
    }
}
