﻿namespace Proovitöö_Netgroup
{
    public class Stock
    {
        public string SecurityKey { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }
    }
}
