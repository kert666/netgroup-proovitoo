﻿using Proovitöö_Netgroup.Helper;

namespace Proovitöö_Netgroup
{
    class Program
    {
        static void Main(string[] args)
        {
            Reconciliation reconciliation = new Reconciliation();
            reconciliation.CompareAssets();
        }
    }
}
