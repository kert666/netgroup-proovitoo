﻿using Proovitöö_Netgroup.Enum;
using System;
using System.Collections.ObjectModel;

namespace Proovitöö_Netgroup.Helper
{
    public class Reconciliation
    {
        private readonly Portfolio _portfolio;

        public Collection<FondEtf> FondEtfs { get; set; }

        public Collection<FondStock> FondStocks { get; set; }

        public FondOtherAsset FondOtherAsset { get; set; }

        public Reconciliation()
        {
            _portfolio = new Portfolio();

            FillFondAssetData();
        }

        public void CompareAssets()
        {
            foreach (FondEtf fondEtf in FondEtfs)
            {
                Etf etfPosition = _portfolio.EtfPosition(fondEtf.SecurityKey);

                if (etfPosition == null || etfPosition.Price * etfPosition.Quantity != fondEtf.MarketValue)
                {
                    Console.WriteLine($"{fondEtf.SecurityKey} {StatusCode.NOT_OK}");
                }
                else
                {
                    Console.WriteLine($"{fondEtf.SecurityKey} {StatusCode.OK}");
                }
            }

            foreach (FondStock fondStock in FondStocks)
            {
                Stock stockPositions = _portfolio.StockPosition(fondStock.SecurityKey);

                if (stockPositions == null || stockPositions.Price * stockPositions.Quantity != fondStock.MarketValue)
                {
                    Console.WriteLine($"{fondStock.SecurityKey} {StatusCode.NOT_OK}");
                }
                else
                {
                    Console.WriteLine($"{fondStock.SecurityKey} {StatusCode.OK}");
                }
            }

            Console.WriteLine(_portfolio.OtherAsset.MarketValue != FondOtherAsset.MarketValue
                ? $"Other asset {StatusCode.NOT_OK}"
                : $"Other asset {StatusCode.OK}");
        }

        private void FillFondAssetData()
        {
            FondEtfs = new Collection<FondEtf>
            {
                new FondEtf
                {
                    SecurityKey = "EBB444",
                    MarketValue = 10000m
                },

                new FondEtf
                {
                    SecurityKey = "EBC",
                    MarketValue = 50000m
                }
            };

            FondStocks = new Collection<FondStock>
            {
                new FondStock
                {
                    SecurityKey = "SBC",
                    MarketValue = 8400.00m
                },

                new FondStock
                {
                    SecurityKey = "SBB",
                    MarketValue = 33500m
                }
            };

            FondOtherAsset = new FondOtherAsset
            {
                MarketValue = 1000000.00m
            };
        }
    }
}
